﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriangleWaveGenerator : SampleGenerator
{
    public override float GetSample(int sample, AudioGen soundGenerator)
    {
        float sampleSaw = ((sample * (2 * soundGenerator.Frequency / soundGenerator.SampleRate)) % 2);
        float sampleValue = 2 * sampleSaw;
        if (sampleValue > 1)
            sampleValue = 2 - sampleValue;
        if (sampleValue < -1)
            sampleValue = -2 - sampleValue;

        return sampleValue * soundGenerator.Amplitude;
    }
}