﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SawWaveGenerator : SampleGenerator
{
    public override float GetSample(int sample, AudioGen soundGenerator)
    {
        return soundGenerator.Amplitude * (((sample * (2 * soundGenerator.Frequency / soundGenerator.SampleRate)) % 2) - 1);
    }
}