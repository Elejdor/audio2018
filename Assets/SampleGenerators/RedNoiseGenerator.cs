﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

public class RedNoiseGenerator : SampleGenerator
{
    private double lastOutput;
    private Random r;

    public RedNoiseGenerator() {
        r = new Random();
    }

    public override float GetSample(int sample, AudioGen soundGenerator)
    {
        double white = (r.Next(2000) - 1000) * 0.001f;
        double red = (lastOutput + (0.02 * white)) / 1.02;
        lastOutput = red;
        return Mathf.Clamp((float)(soundGenerator.Amplitude * red * 3.5), -soundGenerator.Amplitude, soundGenerator.Amplitude);
    }
}