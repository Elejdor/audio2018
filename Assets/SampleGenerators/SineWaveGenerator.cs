﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SineWaveGenerator : SampleGenerator
{
    public override float GetSample(int sample, AudioGen soundGenerator)
    {
        return soundGenerator.Amplitude * Mathf.Sin(Mathf.PI*4 * soundGenerator.Frequency * sample / soundGenerator.SampleRate);
    }
}