﻿using System;

public class WhiteNoiseGenerator : SampleGenerator
{
    private Random r;

    public WhiteNoiseGenerator()
    {
        r = new Random();
    }

    public override float GetSample(int sample, AudioGen soundGenerator)
    {
        lock (r)
        {
            return soundGenerator.Amplitude * ((r.Next(2000) - 1000) * 0.001f);
        }
        
    }
}