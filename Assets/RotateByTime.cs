﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateByTime : MonoBehaviour
{
    [Range(0,360)]
    public float m_AngularSpeed = 120;
	// Use this for initialization
	void Start () {
		
	}

    private float angle = 0;
	// Update is called once per frame
	void Update ()
	{
	    angle += Time.deltaTime * m_AngularSpeed;
	    if (angle > 360f) angle -= 360f;
	    transform.rotation = Quaternion.Euler(0, angle, 0);
	}
}
