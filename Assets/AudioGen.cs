﻿using FMOD;
using FMODUnity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UBug = UnityEngine.Debug;

public abstract class SampleGenerator {
    public abstract float GetSample(int sample, AudioGen soundGenerator);
}

public enum GeneratorType : int
{
    Sine,
    WhiteNoise,
    Square,
    Saw,
    Triangle,
    RedNoise
}

public class AudioGen : MonoBehaviour {

    [SerializeField]
    [Range(0,2000)]
    private float m_freq = 440;
    private int m_sr = 44100; // sample rate
    private int m_sampleNumber = 0;

    private const int m_channels = 1;

    private Vector3 m_previousPositionInWorld;

    private CREATESOUNDEXINFO m_soundInfo;
    private FMOD.System m_system;
    private ChannelGroup m_channelGroup;
    private Channel m_channel;

    private Sound m_sound;

    private SampleGenerator [] m_sampleGenerators;

    private short[] m_buffer;

    public GeneratorType m_GeneratorType = GeneratorType.Square;

    public float Amplitude { get; private set; }
    public float MaxAmplitude { get; private set; }

    public float Frequency {
        get { return m_freq; }
        private set { m_freq = value; }
    }

    public int SampleRate {
        get { return m_sr; }
        private set { m_sr = value; }
    }

    protected void Awake()
    {
        MaxAmplitude = DecibelsToAmplitude(100);
        m_buffer = new short[0];
    }

    // Use this for initialization
    void Start()
    {
        m_sampleGenerators = new SampleGenerator[6];
        m_sampleGenerators[0] = new SineWaveGenerator();
        m_sampleGenerators[1] = new WhiteNoiseGenerator();
        m_sampleGenerators[2] = new SquareWaveGenerator();
        m_sampleGenerators[3] = new SawWaveGenerator();
        m_sampleGenerators[4] = new TriangleWaveGenerator();
        m_sampleGenerators[5] = new RedNoiseGenerator();
        SetVolume(80);

        m_system = RuntimeManager.LowlevelSystem;
        UBug.Log("Get group: " + m_system.getMasterChannelGroup( out m_channelGroup ));
        
        m_soundInfo = new CREATESOUNDEXINFO();
        m_soundInfo.cbsize = Marshal.SizeOf( m_soundInfo );
        m_soundInfo.decodebuffersize = (uint)m_sr / 10; //100ms

        m_soundInfo.length = (uint)( m_sr * m_channels * sizeof( short ) );
        m_soundInfo.numchannels = m_channels;
        m_soundInfo.defaultfrequency = m_sr;
        m_soundInfo.format = SOUND_FORMAT.PCM16;

        m_soundInfo.pcmreadcallback = OnPcmReadCallback;
        m_soundInfo.pcmsetposcallback = OnPcmSetPositionCallback;

        UBug.Log("Create stream: " + m_system.createStream("GeneratedSound", MODE.OPENUSER, ref m_soundInfo, out m_sound));

        m_sound.setMode(MODE.OPENUSER | MODE._3D | MODE._3D_LINEARSQUAREROLLOFF);
        m_sound.set3DMinMaxDistance(0, 40);

        m_system.playSound(m_sound, m_channelGroup, true, out m_channel);
        m_channel.setLoopCount(-1);
        m_channel.setMode(MODE.LOOP_NORMAL);
        m_channel.setPosition(0, TIMEUNIT.MS);


        

        m_channel.setPaused(false);
    }

    // Update is called once per frame
    void Update()
    {
        VECTOR position = transform.position.ToFMODVector();
        VECTOR velocity = ((transform.position-m_previousPositionInWorld)*100).ToFMODVector();
        VECTOR altPanPos = Vector3.zero.ToFMODVector();
        m_channel.set3DAttributes(ref position, ref velocity, ref altPanPos);
        m_previousPositionInWorld = transform.position;
    }

    private RESULT OnPcmReadCallback( IntPtr soundraw, IntPtr data, uint rawLength )
    {
        int length = ((int)rawLength) / sizeof(short);
        if (length > m_buffer.Length) {
            m_buffer = new short[length];
        }

        int i = 0;
        SampleGenerator s = m_sampleGenerators[(int) m_GeneratorType];
        lock (s)
        {
            for (i = 0; i < length; ++i)
            {
                float generated = s.GetSample(m_sampleNumber, this)/MaxAmplitude;
                short sampleValue = (short)Mathf.RoundToInt(generated * short.MaxValue);
                m_buffer[i] = sampleValue;

                ++m_sampleNumber;
                if (m_sampleNumber >= m_sr) {
                    m_sampleNumber = 0;
                }
            }
        }
        

        Marshal.Copy(m_buffer, 0, data, length);

        return RESULT.OK;
    }

    private RESULT OnPcmSetPositionCallback( IntPtr soundraw, int subsound, uint position, TIMEUNIT postype )
    {
        return RESULT.OK;
    }

    public void SetVolume(float dB)
    {
        Amplitude = DecibelsToAmplitude(dB);
    }

    public void SetAmplitude(float amp)
    {
        Amplitude = amp;
    }

    public static float DecibelsToAmplitude(float dB)
    {
        return Mathf.Pow(10, dB / 20f);
    }
}
