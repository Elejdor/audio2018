﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class Footsteps : MonoBehaviour
{
    FirstPersonController m_pc;

    string m_eventName = "event:/Footsteps";

    void Start () {
        m_pc = GetComponent<FirstPersonController>();
        m_pc.m_onFootstep = PlayStep;
	}

    void PlayStep()
    {
        FMODUnity.RuntimeManager.PlayOneShot( m_eventName );
    }
}
